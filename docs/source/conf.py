# Configuration file for the Sphinx documentation builder.

# -- Project information

project = "Gradema"
copyright = "2024, Lavender Shannon"
author = "Lavender Shannon"

# release = '0.1'
# version = '0.1.0'

# -- General configuration

extensions = [
    "sphinx.ext.duration",
    "sphinx.ext.doctest",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.intersphinx",
    "sphinx.ext.extlinks",
    "autoapi.extension",
]

intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
    "sphinx": ("https://www.sphinx-doc.org/en/master/", None),
}
intersphinx_disabled_domains = ["std"]


autoapi_dirs = ["../../gradema"]
# https://sphinx-autoapi.readthedocs.io/en/latest/reference/config.html#customisation-options
autoapi_options = [
    "members",
    "undoc-members",
    # "private-members",
    "show-inheritance",
    "show-module-summary",
    "special-members",
    "imported-members",
]
autoapi_root = "module"  # NOTE: IMPORTANT: The module/ directory will be removed. This is why we use module.rst and not module/index.rst!!!!
autoapi_add_toctree_entry = False  # now module/index won't be autogenerated for us (we use our own custom module.rst instead of their module/index.rst anyway)
templates_path = ["_templates"]
# autoapi_keep_files = True
# -- Options for HTML output

# html_theme = 'sphinx_rtd_theme'
html_theme = "furo"

# html_theme_options = {
# }
html_theme_options = dict()  # type: ignore

# -- Options for EPUB output
epub_show_urls = "footnote"


extlinks = {
    "issue-page": ("https://gitlab.com/classroomcode/gradema/gradema/-/issues%s", "issues%s"),
    "issue": ("https://gitlab.com/classroomcode/gradema/gradema/-/issues/%s", "#%s"),
    "blob": ("https://gitlab.com/classroomcode/gradema/gradema/-/blob/%s", "gradema/blob/%s"),
    "tree": ("https://gitlab.com/classroomcode/gradema/gradema/-/tree/%s", "gradema/tree/%s"),
}

html_sourcelink_suffix = ""
