grade.sh
===========

In order to fully understand all the design of Gradema, it's worth looking at the history of `grade.sh <https://gitlab.com/classroomcode/grade-sh/grade-sh>`_.


Original grade.sh features
-----------------------------

Debugging
^^^^^^^^^^^

All of these features have their own ways to debug them.

* Unit tests debugger launch: https://gitlab.com/classroomcode/grade-sh/grade-sh/-/blob/2d4b707e6288f356c69c1d1edcc44014c0e66c72/.admin_files/grade_backend.sh#L225
* Stdio test debugger launch: https://gitlab.com/classroomcode/grade-sh/grade-sh/-/blob/2d4b707e6288f356c69c1d1edcc44014c0e66c72/.admin_files/grade_backend.sh#L315

  * Also can "debug" by diffing https://gitlab.com/classroomcode/grade-sh/grade-sh/-/blob/2d4b707e6288f356c69c1d1edcc44014c0e66c72/.admin_files/grade_backend.sh#L286

* Arg tests debugger launch: https://gitlab.com/classroomcode/grade-sh/grade-sh/-/blob/2d4b707e6288f356c69c1d1edcc44014c0e66c72/.admin_files/grade_backend.sh#L428
* Cfg tests do not have a debug option
* Doctests test debugger launch: https://gitlab.com/classroomcode/grade-sh/grade-sh/-/blob/2d4b707e6288f356c69c1d1edcc44014c0e66c72/.admin_files/grade_backend.sh#L569
* files exists tests do not have a debug option

The sections of grade.sh
----------------------------

Static analysis (type hinting)

* https://gitlab.com/classroomcode/grade-sh/grade-sh/-/blob/2d4b707e6288f356c69c1d1edcc44014c0e66c72/.admin_files/grade_backend.sh#L658

Code formatting

* https://gitlab.com/classroomcode/grade-sh/grade-sh/-/blob/2d4b707e6288f356c69c1d1edcc44014c0e66c72/.admin_files/grade_backend.sh#L702


Start of "main":
* https://gitlab.com/classroomcode/grade-sh/grade-sh/-/blob/2d4b707e6288f356c69c1d1edcc44014c0e66c72/.admin_files/grade_backend.sh#L646

  * Starts by calling unit tests, stdio tests, arg tests, doctest tests, cfg_tests, files_exist
  * Then jumps into static analysis, format check, variable custom tests

Grading:
* https://gitlab.com/classroomcode/grade-sh/grade-sh/-/blob/2d4b707e6288f356c69c1d1edcc44014c0e66c72/.admin_files/grade_backend.sh#L766

  * `$grade` stores the total grade, which is then divided by the number of tests and floored

