PyPI Publishing
==================

Gradema is on PyPI! https://pypi.org/project/gradema/

But how did it get there?
Uploading projects to PyPI is fairly simple, and `poetry <https://python-poetry.org>`_ makes that `super easy <https://python-poetry.org/docs/libraries#publishing-to-pypi>`_.

At some point it would be nice to have GitLab CI/CD do the work for us, but for now, the manual uploading is a fairly simple process:

.. code-block:: shell

  # Configure your token:
  poetry config pypi-token.pypi <my-token>
  poetry publish --build

Yeah! It's that simple. Just make sure to bump the version number to be correct before publishing!
