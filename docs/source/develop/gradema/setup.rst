Environment Setup
====================

If you want to work on Gradema, it's probably a good idea to have a nice development environment you are comfortable in.
There are some requirements to get this project to build, and then some recommendations we have for using an IDE.

Building
------------

This project is managed by [poetry](https://python-poetry.org).
To build the project, first [install poetry](https://python-poetry.org/docs/#installation).

It's important to follow the official poetry install instructions,
and it's equally important to make sure that the virtual environment you use for Gradema is different from the virtual environment poetry is installed in.

Once installed, you can try out the included example assignment:

.. code-block:: shell

  poetry install
  poetry run python -m example.binaryconvert.autograder


IDE Setup
------------

* PyCharm: https://www.jetbrains.com/help/pycharm/poetry.html

  * File > Settings > Project: gradema > Python Interpreter > Add Interpreter > Add Local Interpreter
  * When choosing or creating an interpreter, make sure to create a Poetry interpreter for best IDE integration
  * Create a new Poetry interpreter, even if you've already created one

    * When asked for your Poetry binary

      * On Linux it will be likely be either `~/.local/bin/poetry` (assuming you installed it using pipx)

* VS Code: https://github.com/microsoft/vscode-python/wiki/Support-poetry-virtual-environments
