Usage of Gradema
===================

Obviously you're going to use Gradema outside of just the Gradema repository itself.
The whole idea behind Gradema is that it is published on PyPI and it uses semantic versioning.
This means that there should be very little redundancy between assignment repositories.
(No putting the entirety of ``grade_backend.sh`` in each and every repository).

Declaring Gradema as a dependency
-----------------------------------

Gradema needs to be a dependency in your assignment repository,
no matter if you are configuring an autograder to grade Python code or Rust code.
This means that every repository will have some sort of script or dependency manifest that defines what version of Gradema it depends on.
It is recommended to use ``pyproject.toml`` for this case, but ``requirements.txt`` is also an option.


It is most important that ``pyproject.toml`` contain ``gradema==x.x.x`` as a dependency,
but you can also configure metadata as described here: https://packaging.python.org/en/latest/tutorials/packaging-projects/#configuring-metadata


