Assignment Development
========================

Contains documentation about assignments and how to create and structure them.
This should be easily digestible by anyone who needs to create assignments.


.. toctree::
  :maxdepth: 2

  security
  usage
  windows
