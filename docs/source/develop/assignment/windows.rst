Windows Considerations
========================

It would be ideal if we can get the autograder to work consistently on Windows.
This page helps document some gotchas that are useful to understand.


Line Endings
---------------

That's right! Line endings!
By default, Git for Windows will checkout the repository with CRLF endings and then commit those changes with LF endings.
This is good sometimes: it allows text editors to happily deal with CRLF endings and then commit those as consistent LF endings.

Diffs
^^^^^^

CRLF endings are a problem when it comes to diffs.
The current implementation of our strict diffing requires line endings to be consistent between targets.
The easiest way around this is to make sure that ``.gitattributes`` tells Git for Windows whether to check them out with CRLF line endings on Windows or not.
Before you say, well yeah, let's just always use LF endings, we need to keep in mind that some programming languages will automatically use the platform's native line endings.
For instance, Python will use CRLF endings by default on Windows. (See `open() <https://docs.python.org/3/library/functions.html#open>`_).
However, Rust does not try to automatically use the line endings of the platform it is running on. (See https://stackoverflow.com/questions/47541191/ ).

To solve this, we should explicitly tell Git what to do with line endings.

For a Python assignment, we want files checked out on a Windows machine to have CRLF endings, so we put this in ``.gitattributes``

.. code-block::

  * text=auto

For a Rust assignment, we want files to be checked out with LF line endings, so we put this in ``.gitattributes``

.. code-block::

  * text eol=lf

Now, we have to realize that both of these configurations tell Git that it is allowed to alter line endings.
Even if we are using ``* text eol=lf``, Git would check out a file committed with CRLF line endings using LF line endings.
It's unlikely that we would ever have a text file with CRLF endings, but if for some reason we wanted to prevent Git from altering line endings at all, we could use this:

.. code-block::

  * binary

Now, we should **never use this**.
By saying that all files are binary, Git loses the ability to perform diffs on these files.
If we really need this, we could consider doing something like ``*.txt binary`` so that the goal files do not have their line endings converted.


File exists tests
^^^^^^^^^^^^^^^^^^^^^

The file exists test uses the same library that ``file`` does under the hood.
Just consider this output:

.. code-block:: console

  $ echo -e "Hello\nthere" > example.txt
  $ file example.txt
  example.txt: ASCII text
  $ unix2dos example.txt
  $ file example.txt
  example.txt: ASCII text, with CRLF line terminators

As you can see, the output adds ``with CRLF line terminators``.
Now, this isn't a problem *at the moment*. This is because the current logic only checks to make sure the output of the file type **contains** the target file type.
If this becomes a problem in the future, consider using ``mime=True``. For example:

.. code-block:: console

  $ echo -e "Hello\nthere" > example.txt
  $ file --mime example.txt
  example.txt: text/plain; charset=us-ascii
  $ unix2dos example.txt
  $ file --mime example.txt
  example.txt: text/plain; charset=us-ascii

As you can see, when using the mime option, line endings are not specified in the output.
