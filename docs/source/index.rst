Gradema
============

**Gradema** is a framework to make creating programming assignments with autograders easy.

https://gitlab.com/classroomcode/gradema/gradema

https://pypi.org/project/gradema/

https://gradema.readthedocs.io

.. toctree::
  :maxdepth: 2

  student/index
  develop/index
  module

Intro
------

Gradema is a grading framework written in Python that enables you to write autograders for assignments in Python, Rust,
or add your own support for a language!
You specify the sections of your assignment in a declarative way, and Gradema will create an interactive and easy to understand environment for running your tests!
