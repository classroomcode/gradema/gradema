Student Documentation
=======================

Contains documentation that students can use to set up their environment and user the autograder.

.. toctree::
  :maxdepth: 2

  windows/index
