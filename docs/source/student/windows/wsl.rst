Windows Subsystem for Linux
===============================

This page assumes you have gone through :doc:`setup`.

This page is work in progress.
Once complete, this should give students an idea of how to setup their Windows Subsystem for Linux,
and then it should link them to the Linux specific instructions.
