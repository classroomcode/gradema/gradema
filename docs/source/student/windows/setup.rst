Windows Setup
===============

This goes over steps that you should do assuming you want to develop your code on a program installed on your Windows machine.
The general recommendation is to follow these steps and then choose either run the autograder using Windows natively, or use Windows Subsystem for Linux (WSL).

Install
----------

Things to install:

.. code-block::

  winget install --id Git.Git -e --source winget


Cloning
--------

You should have a pretty good idea of how to clone your repository.
Go ahead and clone your repository in your location of choice.
You can use a command line tool to do so such as PowerShell or Git Bash.

.. code-block:: shell

  cd Documents/somewhereIWannaCloneThis
  # If you have an SSH Key setup with GitLab (recommended)
  git clone git@git-classes.mst.edu:202X-XS-CSXXXX/202X-FS-A-pa0X_XXXXX-abc123.git
  # or if you want to configure a token (not recommended)
  git clone https://git-classes.mst.edu/202X-XS-CSXXXX/202X-FS-A-pa0X_XXXXX-abc123.git


.. _ide_integration:

Integrating with your IDE
----------------------------

Now that you have this cloned locally, you should be able to integrate this with your IDE installed on your Windows machine.
You may be wondering, why the extra steps to integrate into my IDE?
Most IDEs have powerful autocomplete features that require the IDE itself to understand the dependencies and structure of the project.

In the case of a project that uses Rust, let the IDE do all of it's magic by interpreting the ``Cargo.toml`` file.

In the case of a project that uses Python, it's generally best to do go through the :doc:`native-windows` steps in order
to create a virtual environment which makes the autograder easy to run, and gets all the dependencies installed for your assignment.
If you still want to use WSL, you can, but you first need to go through the :doc:`native-windows` steps to have good IDE integration.

