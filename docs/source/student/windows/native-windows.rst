Native Windows Install
==========================

So you want to run the autograder on your Windows machine?
This page will help you get started.

This page assumes you have gone through :doc:`setup`.

.. note::

  If you want a Linux like environment on your Windows machine, you should instead look into WSL (Windows subsystem for Linux)

  This page does not help you setup your environment for WSL. If you want to use WSL, check out :doc:`wsl`.

Install
----------

Things to install:

.. code-block::

  # A Python version >= 3.10:
  winget install Python.Python.3.12

Alternatively, install Git here: https://git-scm.com/download/win

Alternatively, install Python here: https://www.python.org/downloads/windows/ (make sure to add python.exe to your path if you do this)


Development Environment Setup
------------------------------

You now should have cloned your repo and your machine should have tools such as Python and Git installed.
Make sure your working directory is that of your repository.

.. code-block:: shell

  cd 202X-FS-A-pa0X_XXXXX-abc123

When installing Python on Windows, it comes with venv (virtual environment).
We can use one of the scripts that should be present in the repository you have cloned.

For a Command Prompt/PowerShell:

.. code-block:: powershell

  .\setup_venv.bat

.. note::

  If you get an error when using PowerShell, you may need to enable the running of scripts on your system: https://stackoverflow.com/a/64633728/5434860

For a Git Bash shell:

.. code-block:: shell

  ./setup_venv.bat

All of these shells will indicate that the virtual environment is activated with a ``(venv)`` at the beginning of the line.

Now that a virtual environment has been created, let's activate it

For a Command Prompt/PowerShell:

.. code-block:: powershell

  .\.venv\Scripts\activate

For a Git Bash shell:

.. code-block:: shell

  .venv/Scripts/activate

Now your environment should be set up!

IDE Integration
-------------------

Integrating your IDE with the virtual environment is only necessary if your project is using Python.
If your project does not involve Python, there is no reason to integrate the virtual environment with your IDE.

You may refer back to :ref:`ide_integration` should you need to integrate your IDE with a language besides Python.

If you would like to integrate your IDE with the Python virtual environment, check out these resources:

* Visual Studio Code: https://code.visualstudio.com/docs/python/environments#_working-with-python-interpreters
* PyCharm: https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html

Running ``grade.bat``
----------------------

.. warning::

  The result from running the autograder directly on Windows may be different than the result of running on a Linux distrubtion.
  Remember that your actual grade is determined by running the autograder inside GitLab, which uses a Linux distribution.

Your repository should contain files called ``grade.sh`` and ``grade.bat``.
``grade.sh`` is used on Linux, and is what is used by GitLab CI/CD to autograde your assignment when you push changes.
Since you are on Windows, you must use ``grade.bat``.
Running ``grade.bat`` is very simple:

For a Command Prompt/PowerShell:

.. code-block:: powershell

  .\grade.bat

For a Git Bash shell:

.. code-block:: shell

  winpty ./grade.bat


.. note::

  The rest of this document is for those curious about technical details about the autograder.
  You may skip the rest of this page if you wish.

Problems with the autograder on Windows
------------------------------------------

As we warned earlier, the result of running the autograder in Windows is not always consistent with that of running it on Linux.
Being able to run the autograder on Windows is a convenience, and should not be completely trusted to be correct.

This section might be more technical than necessary for most readers, so feel free to skip it!

Diff tests and Line Endings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you run the autograder and notice failing diff tests but don't see anything different, it may be because of line endings.
Line endings are a topic for another day, but basically Windows uses ``\r\n`` (CR+LF) while other operating systems use ``\n`` (LF).
When you clone your Git repository, Git will, by default, change all LF line endings to CR+LF line endings so that text editors on Windows use consistent line endings.
This is a problem only when using languages such as Rust that do not write files with the new line type for the given platform it is running on.

Errors like this will only happen if ``.gitattributes`` is not correctly configured for the given project. (If you get this error, you should report it to Dr. Taylor).

If you run into errors like this, it's best to check the diff yourself in the web browser view, and then confirm that the tests pass in GitLab CI/CD.

Extra Enhancements
--------------------

Stop ``python3`` command from opening Microsoft Store
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you have installed Python using a method other than through the Microsoft Store,
typing ``python3`` will launch the Microsoft Store because only the ``python`` command resolves to the Python executable.

To remove the alias, go to Settings > Manage App execution aliases and disable aliases for ``python.exe`` and ``python3.exe``.

If you are using PowerShell and want ``python3`` to resolve to ``python``, see this answer: https://superuser.com/a/1603110
If you are using Git Bash (not recommended), you can create an alias in your ``~/.bash_profile``.


Notes if you use Git Bash
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default, most people on Windows will use Command Prompt or PowerShell as their shell.
Some may choose to use Git Bash, which is a shell that is more similar to the shells you use on Linux.
It's worth noting that the running ``python`` by itself has some weirdness. See here for details: https://stackoverflow.com/a/36530750/5434860

We recommend that you use ``winpty python.exe ...`` if you ever need to execute any ``python`` command.
Note that as of writing, scripts such as ``grade.bat`` do not do this, so consider doing ``winpty ./grade.bat`` if you want to run the autograder in Git Bash.
