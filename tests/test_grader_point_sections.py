from gradema.grader._grader import point_sections
from gradema.section import Section
from gradema.test import dummy_test


def test_point_sections_evenly_weighted() -> None:
    test = dummy_test()
    assert point_sections(
        100,
        [
            Section.evenly_weighted("", test),
            Section.evenly_weighted("", test),
        ],
    ) == [50, 50]

    assert point_sections(
        99,
        [
            Section.evenly_weighted("", test),
            Section.evenly_weighted("", test),
        ],
    ) == [49, 50]


def test_point_sections_pointed_and_evenly_weighted() -> None:
    test = dummy_test()
    assert point_sections(
        100,
        [
            Section.pointed(80, "", test),
            Section.evenly_weighted("", test),
        ],
    ) == [80, 20]

    assert point_sections(
        99,
        [
            Section.pointed(80, "", test),
            Section.evenly_weighted("", test),
        ],
    ) == [80, 19]

    assert point_sections(
        99,
        [
            Section.pointed(80, "", test),
            Section.pointed(10, "", test),
            Section.evenly_weighted("", test),
        ],
    ) == [80, 10, 9]

    assert point_sections(
        99,
        [
            Section.pointed(80, "", test),
            Section.ungraded("", test),
            Section.pointed(10, "", test),
            Section.evenly_weighted("", test),
            Section.evenly_weighted("", test),
        ],
    ) == [80, 0, 10, 4, 5]


def test_point_sections_weighted() -> None:
    test = dummy_test()

    # The total weights add up to over 1 here, but that's OK because we don't have any evenly weighted sections
    assert point_sections(
        100,
        [
            Section.pointed(80, "", test),
            Section.weighted(1.0, "", test),
            Section.weighted(1.0, "", test),
        ],
    ) == [80, 10, 10]

    assert point_sections(
        100,
        [
            Section.pointed(80, "", test),
            Section.weighted(0.75, "", test),
            Section.evenly_weighted("", test),
        ],
    ) == [80, 15, 5]

    assert point_sections(
        100,
        [
            Section.pointed(80, "", test),
            Section.weighted(0.80, "", test),
            Section.weighted(0.05, "", test),
            Section.evenly_weighted("", test),
        ],
    ) == [80, 16, 1, 3]
