from gradema.grader.console._util import indent_text, indentation


def test_indent_text() -> None:
    text = (
        'Traceback (most recent call last):\n  File "/home/lavender/programming/Mst/CS4000Taylor/Gradema/gradema/gradema/test/_exists.py", line 28, in run\n    output = '
        'magic.from_file(path)\n  File "/home/lavender/.cache/pypoetry/virtualenvs/gradema-_P6rAskr-py3.10/lib/python3.10/site-packages/magic/__init__.py", line 179, '
        'in from_file\n    return m.from_file(filename)\n  File "/home/lavender/.cache/pypoetry/virtualenvs/gradema-_P6rAskr-py3.10/lib/python3.10/site-packages/magic'
        "/__init__.py\", line 112, in from_file\n    with _real_open(filename):\nFileNotFoundError: [Errno 2] No such file or directory: 'does not exist.txt'\n"
    )
    indented = indent_text(2, text)
    assert indented.startswith(indentation(2) + "T")
