# autograder

This directory contains other Python modules that import gradema.
The modules in this directory are for showing usage of gradema.
The modules within this directory are not meant to be used as direct examples to use
because it doesn't perfectly show the exact structure and usage gradema as would be shown in a dedicated example or template repository.

Remember that this is a module, so any testing, running, or importing of this module or modules inside this module must refer to the entire path of the module.
For instance, to run the autograder for binary convert, you must refer to `example.binaryconvert.autograder`.

This will take inspiration from https://gitlab.com/classroomcode/grade-sh
