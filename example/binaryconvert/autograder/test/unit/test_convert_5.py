from example.binaryconvert.submission.convert_to_binary import convert_to_binary


def test_convert_5() -> None:
    from pathlib import Path

    # This code is included to show that it does not bypass the grader's writing of results.txt
    # This code does not affect the results of this test and could easily be placed inside of convert_to_binary (a function that the student writes)
    # NOTE: This will cause a PermissionError on windows (because Gradema will call unlink on a read-only file)
    #   But this isn't a problem in Linux environments.
    results = Path("results.txt")
    with results.open("w") as file:
        print("100", file=file)
    results.chmod(0o400)
    assert convert_to_binary(5) == "101"
