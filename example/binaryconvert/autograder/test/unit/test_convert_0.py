from example.binaryconvert.submission.convert_to_binary import convert_to_binary

if __name__ == "__main__":
    assert convert_to_binary(0) == "0"
