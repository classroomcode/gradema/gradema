import sys
from pathlib import Path

from gradema.grader.console import run_grader
from gradema.section import Section
from gradema.test import (
    create_python_test,
    dummy_test,
    create_python_format_check,
    create_python_type_check,
    create_python_stdio_test,
    create_python_traditional_stdio_test,
    create_file_exists_test,
    create_python_traditional_arg_test,
    create_python_pytest,
    create_mypy_command,
)
from gradema.test.argument import OUTPUT_FILE


def main(args: list[str]) -> int:
    print("Welcome to the autograder that uses gradema")
    test_directory = Path("example/binaryconvert/autograder/test/")
    section = Section.pointed(
        100,
        "Binary Convert Programming Assignment",
        [
            Section.ungraded("Compile", dummy_test()),
            Section.evenly_weighted(
                "Unit Tests",
                [
                    Section.evenly_weighted("Convert 0", create_python_test("example.binaryconvert.autograder.test.unit.test_convert_0")),
                    Section.evenly_weighted("Convert 1 Pytest", create_python_pytest("example/binaryconvert/autograder/test/unit/test_convert_1.py::test_convert_1")),
                    Section.evenly_weighted("Convert 5 Pytest", create_python_pytest("example/binaryconvert/autograder/test/unit/test_convert_5.py::test_convert_5")),
                ],
            ),
            Section.evenly_weighted(
                "Standard Input/Output Tests",
                [
                    Section.evenly_weighted(
                        "Convert 7 Stdio",
                        create_python_traditional_stdio_test(
                            "example.binaryconvert.submission",
                            "convert_7",
                            test_directory / "stdio/inputs/decimal_7.txt",
                            test_directory / "stdio/goals/binary_7.txt",
                        ),
                    ),
                    Section.evenly_weighted(
                        "Convert 13 Stdio",
                        create_python_traditional_stdio_test(
                            "example.binaryconvert.submission",
                            "convert_13",
                            test_directory / "stdio/inputs/decimal_13.txt",
                            test_directory / "stdio/goals/binary_13.txt",
                        ),
                    ),
                ],
            ),
            Section.evenly_weighted(
                "Args Tests",
                [
                    Section.evenly_weighted(
                        "Convert 19 Arg",
                        create_python_traditional_arg_test(
                            "example.binaryconvert.submission",
                            "convert_19_arg",
                            [str(test_directory / "stdio/inputs/decimal_19.txt"), OUTPUT_FILE],
                            test_directory / "stdio/goals/binary_19.txt",
                        ),
                    ),
                ],
            ),
            Section.evenly_weighted(
                "Weird Arg tests",
                [
                    Section.evenly_weighted(
                        "Convert 19 Weird Arg 1",
                        create_python_stdio_test(
                            "example.binaryconvert.submission",
                            "convert_19_weird_arg_1",
                            [str(test_directory / "stdio/inputs/decimal_19.txt"), "-"],
                            None,
                            test_directory / "stdio/goals/binary_19.txt",
                            stdout_as_output=True,
                        ),
                    ),
                    Section.evenly_weighted(
                        "Convert 19 Weird Arg 2",
                        create_python_stdio_test(
                            "example.binaryconvert.submission",
                            "convert_19_weird_arg_2",
                            ["-", OUTPUT_FILE],
                            test_directory / "stdio/inputs/decimal_19.txt",
                            test_directory / "stdio/goals/binary_19.txt",
                            stdout_as_output=False,
                        ),
                    ),
                ],
            ),
            Section.pointed(
                15,
                "Other Checks",
                [
                    Section.evenly_weighted(
                        "Files exist",
                        create_file_exists_test(
                            [
                                ("README.md", "ASCII text"),
                                ("gradema/test/__init__.py", "Python script, ASCII text executable"),
                                ("does not exist.txt", "ASCII text"),
                            ]
                        ),
                    ),
                    Section.evenly_weighted("Formatting", create_python_format_check()),
                    Section.evenly_weighted("Type Checking", create_mypy_command(["--strict", "--disallow-any-explicit", "--exclude", "examples", "."])),
                ],
            ),
        ],
    )
    return run_grader(args, section)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
