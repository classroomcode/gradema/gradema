import sys
from pathlib import Path
from typing import Optional, TextIO

from .convert_to_binary import convert_to_binary


def run_program(input_io: TextIO, output_io: TextIO) -> int:
    try:
        num_to_convert = int(input_io.readline())
    except ValueError:
        print("Invalid input", file=sys.stderr)
        return 1
    print(convert_to_binary(num_to_convert), file=output_io)
    return 0


def main(args: list[str]) -> int:
    if len(args) == 2:
        input_path = args[0]
        output_path = args[1]
        input_io: Optional[TextIO] = None
        close_input = False
        output_io: Optional[TextIO] = None
        close_output = False
        try:
            if input_path == "-":
                input_io = sys.stdin
            else:
                close_input = True
                path = Path(input_path)
                path.parent.mkdir(parents=True, exist_ok=True)
                input_io = path.open("r")

            if output_path == "-":
                output_io = sys.stdout
            else:
                close_output = True
                path = Path(output_path)
                path.parent.mkdir(parents=True, exist_ok=True)
                output_io = path.open("w")
            return run_program(input_io, output_io)
        finally:
            if close_input and input_io is not None:
                input_io.close()
            if close_output and output_io is not None:
                output_io.close()
    elif len(args) == 0:
        return run_program(sys.stdin, sys.stdout)
    else:
        print("Usage:\n  convert_to_binary <input file> <output file>\n  convert_to_binary", file=sys.stderr)
        return 1


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
