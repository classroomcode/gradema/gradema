#!/usr/bin/env sh
# This file is designed to format everything that we want formatted.
# It's worth noting that we might be worth looking into https://black.readthedocs.io/en/stable/usage_and_configuration/the_basics.html#configuration-via-a-file
#   in the future.
poetry run black gradema example tests docs
