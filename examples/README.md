# Examples

This directory contains submodules of repositories that are examples of how to use Gradema.

When developing Gradema there's a good change you'll want to make changes to Gradema and test them with the example projects.

```shell
../gradema-build-install.sh
```
