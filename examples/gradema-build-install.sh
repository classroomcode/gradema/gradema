#!/usr/bin/env sh
# This file is only meant to be used when developing example repositories in this examples/ folder
set -e
base=$(dirname "$0")

(cd "$base/.." && rm -rf dist/ && poetry build)
. .venv/bin/activate
pip install --force-reinstall "$base/../dist"/gradema-*-py3-none-any.whl
#pip install --force-reinstall "$base/../dist"/gradema-*.tar.gz
